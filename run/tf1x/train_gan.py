####################################################
### DEEP LEARNING WITH TF1X                      ###
### GENERATING PREDICTIONS USING A TRAINED MODEL ### 
### by: OAMEED NOAKOASTEEN                       ###
####################################################

import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'
import sys
import numpy             as np
import tensorflow        as tf
sys.path.append(os.path.join(sys.path[0],'..','..','lib'         ))
sys.path.append(os.path.join(sys.path[0],'..','..','lib','models'))
from paramd       import    PARAMS, PATHS
from utilsd       import   (getFILENAMES  ,
                            read_EA_Scalar,
                            plotter        )
from inputd       import   (saveTFRECORDS ,
                            inputTFRECORDS )
from layersd      import    sample_Z
from nndGAN       import   (generator    ,
                            discriminator,
                            loss          )
from tensorboard.backend.event_processing import event_accumulator

TFRTRAIN=os.path.join(PATHS[1],'train')
LOGTRAIN=os.path.join(PATHS[3],'train')
DEVICE  ='/'+PARAMS[0][3]+':'+'0'

#######################
### WRITE TFRECORDS ###
#######################
print(' WRITING TRAINING DATA TO TFRECORDS FORMAT')
saveTFRECORDS(PARAMS[0][0],[PATHS[0],PATHS[1]],PARAMS[0][2],'train')

################
### TRAINING ###
################
with tf.device(DEVICE):
 img, _       =inputTFRECORDS(TFRTRAIN,PARAMS,'train')
 Z            =tf.compat.v1.placeholder(tf.float32,shape=[PARAMS[0][4],PARAMS[0][10]])

 g_sample     =generator    (Z       ,PARAMS[1],'train')
 d_logit_fake =discriminator(g_sample,'Fake'   ,'train')
 d_logit_real =discriminator(img     ,'Real'   ,'train')
 d_loss,g_loss=loss         (d_logit_real,d_logit_fake )
 
 dsr_vars     =[var for var in tf.compat.v1.trainable_variables() if var.name.startswith('dsr')]
 gnr_vars     =[var for var in tf.compat.v1.trainable_variables() if var.name.startswith('gnr')] 
 d_optim      =tf.compat.v1.train.AdamOptimizer().minimize(d_loss, var_list=dsr_vars) 
 g_optim      =tf.compat.v1.train.AdamOptimizer().minimize(g_loss, var_list=gnr_vars) 

VARLIST       =tf.compat.v1.get_collection(tf.compat.v1.GraphKeys.TRAINABLE_VARIABLES)
for i in range(len(VARLIST)):
  tf.compat.v1.summary.histogram(VARLIST[i].name,VARLIST[i])
tf.compat.v1.summary.image ('TRUE'     , img                                                               )
tf.compat.v1.summary.image ('GENERATED', g_sample                                                          )
tf.compat.v1.summary.scalar('d_loss'   , tf.compat.v1.get_collection(tf.compat.v1.GraphKeys.ACTIVATIONS)[0])
tf.compat.v1.summary.scalar('g_loss'   , tf.compat.v1.get_collection(tf.compat.v1.GraphKeys.ACTIVATIONS)[1])

saver         =tf.compat.v1.train.Saver       ()           
summaryMERGED =tf.compat.v1.summary.merge_all ()                        
writer        =tf.compat.v1.summary.FileWriter(LOGTRAIN                 )
sess_config   =tf.compat.v1.ConfigProto       (allow_soft_placement=True)

print(' RUNNING TRAINING SESSION !')
with tf.compat.v1.Session(config=sess_config) as sess:
 sess.run(tf.compat.v1.global_variables_initializer())
 sess.run(tf.compat.v1.local_variables_initializer ()) 
 writer.add_graph(sess.graph)                                         
 try:
  step=0
  while True:
   z              =sample_Z(PARAMS[0][4],PARAMS[0][10])
   feed_dictionary={Z:z}
   step           =step+1   
   if step%PARAMS[0][6]==0:    
    summary=sess.run(summaryMERGED, feed_dict=feed_dictionary)                  
    writer.add_summary(summary,step)                                  
    saver.save(sess, os.path.join(PATHS[2],'model'),global_step=step) 
    print(' TRAINING IN PROGRESS!',' STEP: ',step)                    
   sess.run([d_optim], feed_dict=feed_dictionary)
   sess.run([g_optim], feed_dict=feed_dictionary)
 except tf.errors.OutOfRangeError:
  print(' DONE TRAINING FOR %d EPOCHS, %d STEPS.' %(PARAMS[0][5],step))

#######################
### POST PROCESSING ###
#######################
filenames=getFILENAMES(LOGTRAIN)

EA       =event_accumulator.EventAccumulator(filenames[0]                                                 ,
                                             size_guidance={event_accumulator.COMPRESSED_HISTOGRAMS: 500,
                                                            event_accumulator.IMAGES               : 4  ,
                                                            event_accumulator.AUDIO                : 4  ,
                                                            event_accumulator.SCALARS              : 0  ,
                                                            event_accumulator.HISTOGRAMS           : 1   } )

EA.Reload()                                 # EA.Reload() LOADS EVENTS FROM FILE
                                            # EA.Tags()   GIVES THE KEYS
d_loss=read_EA_Scalar(EA,'d_loss')
g_loss=read_EA_Scalar(EA,'g_loss')
plotter([d_loss,g_loss],LOGTRAIN,'metrics') 


