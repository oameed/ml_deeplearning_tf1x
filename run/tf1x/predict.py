###################################################
### DEEP LEARNING WITH TF1X                     ###
### TRAIN GENERATIVE ADVERSESRIAL NETWORK (GAN) ### 
### by: OAMEED NOAKOASTEEN                      ###
###################################################

import os
import sys
import numpy      as np
import tensorflow as tf
sys.path.append(os.path.join(sys.path[0],'..','..','lib'         ))
sys.path.append(os.path.join(sys.path[0],'..','..','lib','models'))
from paramd  import  PATHS,PARAMS
from utilsd  import (ckptREAD,
                     plotter  )
from layersd import  sample_Z
from nndGAN  import  generator

#################
### REDICTION ###
#################
tf.compat.v1.reset_default_graph()                             # RESET GRAPH
ckpt       =os.path.join(PATHS[2],ckptREAD(PATHS[2],True)    ) # IMPORT GRAPH FROM THE APPROPRIATE CHECKPOINT
saver      =tf.compat.v1.train.import_meta_graph(ckpt+'.meta')

Z          =tf.compat.v1.placeholder(tf.float32,shape=[100,PARAMS[0][10]])
g_sample   =generator(Z,PARAMS[1],'test')

sess_config=tf.compat.v1.ConfigProto(allow_soft_placement=True)  

with tf.compat.v1.Session(config=sess_config) as sess:
 sess.run(tf.compat.v1.global_variables_initializer())
 sess.run(tf.compat.v1.local_variables_initializer()) 
 saver.restore(sess,ckpt)                                      # RESTORE CHECKPOINT 
 z              =sample_Z(100,PARAMS[0][10])
 feed_dictionary={Z:z}
 x              =sess.run(g_sample,feed_dict=feed_dictionary)

x=np.squeeze(x)
x=1-x
plotter(x,PATHS[4],'grid')
 

