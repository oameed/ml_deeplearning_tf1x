#! /bin/bash

source  activate tfpy
cd      run/tf1x

echo ' CREATING PROJECT DIRECTORY '                            'v01'
rm     -rf                                       ../../networks/v01
tar    -xzf  ../../networks/v00.tar.gz -C        ../../networks
mv           ../../networks/v00                  ../../networks/v01
echo ' TRAINING GAN ON MNIST '
python train_gan.py -data MNIST -net v01 -sc 1 -b 128 -eptr 50
echo ' GENERATING PREDICTIONS '
python predict.py   -data MNIST -net v01


