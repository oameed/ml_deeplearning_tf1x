#!/bin/bash

#SBATCH --ntasks=16
#SBATCH --time=48:00:00
#SBATCH --partition=singleGPU
#SBATCH --gres=
#SBATCH --mail-type=BEGIN,FAIL,END
#SBATCH --mail-user=
#SBATCH --job-name=Train_GAN

module load anaconda3
source activate tffpy

cd $SLURM_SUBMIT_DIR
cd ../tf1x

echo ' CREATING PROJECT DIRECTORY '                            'v01'
rm     -rf                                       ../../networks/v01
tar    -xzf  ../../networks/v00.tar.gz -C        ../../networks
mv           ../../networks/v00                  ../../networks/v01
echo ' TRAINING GAN ON MNIST '
python train_gan.py -data MNIST -net v01 -sc 1 -b 128 -eptr 50 -gpu
echo ' GENERATING PREDICTIONS '
python predict.py   -data MNIST -net v01


