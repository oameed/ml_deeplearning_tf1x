# Deep Learning with TensorFlow 1X

This project is Tensorflow 1X implementations of a _Generative Adverserial Network (GAN)_.

## Code Statistics
<pre>
github.com/AlDanial/cloc v 1.74  T=0.03 s (413.8 files/s, 26172.4 lines/s)
-------------------------------------------------------------------------------
Language                     files          blank        comment           code
-------------------------------------------------------------------------------
Python                           7             64             71            380
YAML                             2              2              0            169
Markdown                         1              6              0             26
Bourne Shell                     2             10              7             24
-------------------------------------------------------------------------------
SUM:                            12             82             78            599
-------------------------------------------------------------------------------
</pre>

## How to Run
* This project uses [TensorFlow](https://www.tensorflow.org/) Version 1.14. The `YAML` file for creating the conda environment used to run this project is included in `run/conda` 
* To Run:  
  * `cd` to the project main directory
  *  `./run/sh/train_gan.sh`

## Experiment: v01
Training GAN with MNIST dataset

|     |     |
|:---:|:---:|
|<img src="networks/v01/predictions/predictions.png"  width=929/> | <img src="networks/v01/logs/train/losses.png" width=640/> |

