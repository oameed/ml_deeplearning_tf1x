###################################################
### DEEP LEARNING WITH TF1X                     ###
### TRAIN GENERATIVE ADVERSESRIAL NETWORK (GAN) ### 
### LAYER DEFINITIONS                           ###
### by: OAMEED NOAKOASTEEN                      ###
###################################################

import numpy      as np
import tensorflow as tf

def sample_Z(M,N):
 return (np.random.uniform(-1., 1., size=[M,N])).astype('float32')

def layer_batchnorm(X,NOC,NAME,MODE,SCOPE=[],CONV=True):
 if MODE=='train':
  with tf.compat.v1.variable_scope('BN'+'_'+NAME,reuse=tf.compat.v1.AUTO_REUSE):
   beta =tf.compat.v1.get_variable('beta' ,NOC,initializer=tf.zeros_initializer(),trainable=True)
   gamma=tf.compat.v1.get_variable('gamma',NOC,initializer=tf.ones_initializer() ,trainable=True)
   ema  =tf.train.ExponentialMovingAverage(decay=1-5e-3)
   if CONV:
    batch_mean,batch_var=tf.nn.moments(X,[0,1,2])
   else:
    batch_mean,batch_var=tf.nn.moments(X,[0    ])      
   ema_update_op   =ema.apply([batch_mean,batch_var])
   ema_mean,ema_var=ema.average(batch_mean),ema.average(batch_var)
   with tf.control_dependencies([ema_update_op]):
    x              =tf.nn.batch_normalization(X,batch_mean,batch_var,beta,gamma,1e-3)
 else:
  if MODE=='test':
   with tf.compat.v1.variable_scope('BN'+'_'+NAME,reuse=tf.compat.v1.AUTO_REUSE):
    beta    =tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES,scope=SCOPE+'/'+'BN'+'_'+NAME)[0]
    gamma   =tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES,scope=SCOPE+'/'+'BN'+'_'+NAME)[1]
    ema_mean=tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES   ,scope=SCOPE+'/'+'BN'+'_'+NAME)[2]
    ema_var =tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES   ,scope=SCOPE+'/'+'BN'+'_'+NAME)[3]
    x       =tf.nn.batch_normalization(X,ema_mean,ema_var,beta,gamma,1e-3)
 return x

def layer_conv(X,NOC,STRIDE,NAME,MODE,BN=True,LINEAR=False):
 # STRIDE      =[1,1]
 # STRIDE      =[2,2]
 if MODE=='train':
  NIC        =X.get_shape().as_list()[-1]
  WSHAPE     =[3,3,NIC,NOC]
  BSHAPE     =NOC
  INITIALIZER=tf.contrib.layers.variance_scaling_initializer()
  with tf.compat.v1.variable_scope(NAME+'_'+MODE,reuse=tf.compat.v1.AUTO_REUSE):
   w=tf.compat.v1.get_variable('w',WSHAPE,initializer=INITIALIZER,trainable=True)
   b=tf.compat.v1.get_variable('b',BSHAPE,initializer=INITIALIZER,trainable=True)
   x=tf.nn.conv2d(X,w,strides=[1,STRIDE[0],STRIDE[1],1],padding='SAME')
   x=tf.nn.bias_add(x,b)
   if BN:
    x=layer_batchnorm(x,NOC,'FIRST',MODE)
   if not LINEAR:    
    x=tf.nn.relu(x)
 else:
  if MODE=='test':
   with tf.compat.v1.variable_scope(NAME+'_'+MODE,reuse=tf.compat.v1.AUTO_REUSE):
    w=tf.compat.v1.get_collection(tf.compat.v1.GraphKeys.TRAINABLE_VARIABLES,scope=NAME+'_'+'train')[0]
    b=tf.compat.v1.get_collection(tf.compat.v1.GraphKeys.TRAINABLE_VARIABLES,scope=NAME+'_'+'train')[1]
    x=tf.nn.conv2d(X,w,strides=[1,STRIDE[0],STRIDE[1],1],padding='SAME')
    x=tf.nn.bias_add(x,b)
    if BN:
     x=layer_batchnorm(x,[],'FIRST',MODE,NAME+'_'+'train')
    if not LINEAR:     
     x=tf.nn.relu(x)
 return x

def layer_cvts(X,NOC,NAME,MODE,BN=True,LINEAR=False):
 OUTDIMSFACTOR=2
 XSHAPE       =X.get_shape().as_list()
 OUTSHAPE     =[XSHAPE[0],OUTDIMSFACTOR*XSHAPE[1],OUTDIMSFACTOR*XSHAPE[2],NOC]
 if MODE=='train':
  NIC         =XSHAPE[3]
  WSHAPE      =[3,3,NOC,NIC]
  BSHAPE      =NOC
  INITIALIZER =tf.contrib.layers.variance_scaling_initializer()
  with tf.compat.v1.variable_scope(NAME+'_'+MODE,reuse=tf.compat.v1.AUTO_REUSE):
   w=tf.compat.v1.get_variable('w',WSHAPE,initializer=INITIALIZER,trainable=True)
   b=tf.compat.v1.get_variable('b',BSHAPE,initializer=INITIALIZER,trainable=True)
   x=tf.nn.conv2d_transpose(X,w,OUTSHAPE,strides=[1,2,2,1],padding='SAME')
   x=tf.nn.bias_add(x,b)
   if BN:
    x=layer_batchnorm(x,NOC,'FIRST',MODE)
   if not LINEAR:    
    x=tf.nn.relu(x)
 else:
  if MODE=='test':
   with tf.compat.v1.variable_scope(NAME+'_'+MODE,reuse=tf.compat.v1.AUTO_REUSE):
    w=tf.compat.v1.get_collection(tf.compat.v1.GraphKeys.TRAINABLE_VARIABLES,scope=NAME+'_'+'train')[0]
    b=tf.compat.v1.get_collection(tf.compat.v1.GraphKeys.TRAINABLE_VARIABLES,scope=NAME+'_'+'train')[1]
    x=tf.nn.conv2d_transpose(X,w,OUTSHAPE,strides=[1,2,2,1],padding='SAME')
    x=tf.nn.bias_add(x,b)
    if BN:
     x=layer_batchnorm(x,[],'FIRST',MODE,NAME+'_'+'train')
    if not LINEAR:     
     x=tf.nn.relu(x)
 return x

def layer_flcn(X,OS,NAME,MODE,BN=True,LINEAR=False):
 if MODE=='train':
  IS         =X.get_shape().as_list()[-1]
  WSHAPE     =[IS,OS]
  BSHAPE     =OS
  INITIALIZER=tf.contrib.layers.variance_scaling_initializer()
  with tf.compat.v1.variable_scope(NAME+'_'+MODE,reuse=tf.compat.v1.AUTO_REUSE):
   w=tf.compat.v1.get_variable('w',WSHAPE,initializer=INITIALIZER,trainable=True)
   b=tf.compat.v1.get_variable('b',BSHAPE,initializer=INITIALIZER,trainable=True)
   x=tf.matmul(X,w)
   x=tf.nn.bias_add(x,b)
   if BN:
    x=layer_batchnorm(x,OS,'FIRST',MODE,[],False)
   if not LINEAR:    
    x=tf.nn.relu(x)
 else:
  if MODE=='test':
   with tf.compat.v1.variable_scope(NAME+'_'+MODE,reuse=tf.compat.v1.AUTO_REUSE):
    w=tf.compat.v1.get_collection(tf.compat.v1.GraphKeys.TRAINABLE_VARIABLES,scope=NAME+'_'+'train')[0]
    b=tf.compat.v1.get_collection(tf.compat.v1.GraphKeys.TRAINABLE_VARIABLES,scope=NAME+'_'+'train')[1]
    x=tf.matmul(X,w)
    x=tf.nn.bias_add(x,b)
    if BN:
     x=layer_batchnorm(x,[],'FIRST',MODE,NAME+'_'+'train',False)
    if not LINEAR:     
     x=tf.nn.relu(x)
 return x


