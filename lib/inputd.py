##################################
### DEEP LEARNING WITH TF1X    ###
### INPUT FUNCTION DEFINITIONS ###
### by: OAMEED NOAKOASTEEN     ###
##################################

import os
import sys
import numpy      as np
import tensorflow as tf
from utilsd import   getFILENAMES

def get_X_y(DATASET,PATH,MODE):
 if DATASET=='MNIST':
  mnist=tf.keras.datasets.mnist.load_data()
  if MODE =='train':
   mnist =mnist[0]
  else:
   if MODE=='test' :
    mnist=mnist[1]
 return mnist[0],mnist[1]

def shape_scale_dataset(X,SCALING):
 x=np.reshape(X,[-1,np.prod(X.shape[1:])])
 if SCALING==1:
  maxval=np.amax(x)
  minval=np.amin(x)
  x     =(x-minval)/(maxval-minval)
 return x.astype('float32')

def saveTFRECORDS(DATASET,PATHS,SCALING,MODE):
 def serialize_example(IMG,LAB):  
  feature      ={'img'    : tf.train.Feature(bytes_list=tf.train.BytesList(value=[IMG.tostring()])), 
                 'lab'    : tf.train.Feature(int64_list=tf.train.Int64List(value=[LAB           ])) }
  example_proto=tf.train.Example(features=tf.train.Features(feature=feature))
  return example_proto.SerializeToString()
 savefilename=os.path.join(PATHS[1],MODE,'batch'+'_'+'0'+'.tfrecords')
 X,y         =get_X_y(DATASET,PATHS[0],MODE)
 X           =shape_scale_dataset(X,SCALING)
 with tf.io.TFRecordWriter(savefilename) as writer:
  for i in range(X.shape[0]):
   example=serialize_example(X[i],y[i])
   writer.write(example)

def inputTFRECORDS(PATH,PARAMS,MODE):
 if MODE =='train':
  epoch_num =PARAMS[0][5]
 else:
  if MODE=='test' :
   epoch_num=PARAMS[0][7]
 feature={'img': tf.io.FixedLenFeature([],tf.string),
          'lab': tf.io.FixedLenFeature([],tf.int64 ) }
 def parse_fn(example_proto):
  parsed_example=tf.io.parse_single_example(example_proto        ,feature   )
  img           =tf.io.decode_raw          (parsed_example['img'],tf.float32)
  img.set_shape      ([np.prod(PARAMS[1])])
  img     =tf.reshape(img,     PARAMS[1]  )
  lab     =parsed_example['lab']  
  return img,lab
 with tf.name_scope('READ'+'_'+'TFRecords'+'_'+MODE):
  filenames    =getFILENAMES(PATH)                                                  
  dataset      =tf.data.TFRecordDataset(filenames)
  dataset      =dataset.map            (parse_fn                        )
  dataset      =dataset.shuffle        (PARAMS[0][9]                    )
  dataset      =dataset.repeat         (epoch_num                       )
  dataset      =dataset.batch          (PARAMS[0][4],drop_remainder=True)
  iterator     =tf.compat.v1.data.make_one_shot_iterator(dataset)
  next_element =iterator.get_next()  
 return next_element


