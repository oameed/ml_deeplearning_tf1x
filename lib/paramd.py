###############################
### DEEP LEARNING WITH TF1X ###
### PARAMETER DEFINITIONS   ###
### by: OAMEED NOAKOASTEEN  ###
###############################

import os
import argparse

### DEFINE PARSER
parser=argparse.ArgumentParser() 

### DEFINE PARAMETERS
parser.add_argument('-data'  , type=str  ,                help='NAME-OF-DATA-TYPE-DIRECTORY',required=True         )
parser.add_argument('-net'   , type=str  , default='v00', help='NAME-OF-NETWORK-PROJECT'                           )
parser.add_argument('-sc'    , type=int  , default=1    , help='DATA SCALING SCHEME '                              )
parser.add_argument('-gpu'   ,                            help='ENABLE GPU'                 ,action  ='store_true' )

  # GROUP: HYPER-PARAMETERS FOR TRAINING AND TESTING
parser.add_argument('-b'     , type=int  , default=128  , help='BATCH-SIZE'                                        )
parser.add_argument('-eptr'  , type=int  , default=50   , help='NUMBER-OF-TRAINING-EPOCHS'                         )
parser.add_argument('-steptr', type=int  , default=50   , help='EVERY-TRAIN-STEPS-TO-WRITE-SUMMERIES'              )
parser.add_argument('-epts'  , type=int  , default=1    , help='NUMBER-OF-TESTING-EPOCHS'                          )
parser.add_argument('-stepts', type=int  , default=1    , help='EVERY-TEST-STEPS-TO-WRITE-SUMMERIES'               )
parser.add_argument('-bc'    , type=int  , default=10000, help='BATCH-BUFFER-CAPACITY'                             )

  # GROUP: HYPER-PARAMETERS FOR ARCHITECTURES
parser.add_argument('-zd'    , type=int  , default=100  , help='GAN LATENT DIMENSION'                              )

### ENABLE FLAGS
args=parser.parse_args()

### CONSTRUCT PARAMETER STRUCTURES

PATHS =[os.path.join('..','..','data'    ,args.data             ),
        os.path.join('..','..','networks',args.net,'tfrecords'  ),
        os.path.join('..','..','networks',args.net,'checkpoints'),
        os.path.join('..','..','networks',args.net,'logs'       ),
        os.path.join('..','..','networks',args.net,'predictions') ]

if args.gpu:
 device='gpu'
else:
 device='cpu'

PARAMS=[[args.data,args.net ,args.sc    ,device   ,
         args.b   ,args.eptr,args.steptr,args.epts,args.stepts,args.bc,
         args.zd                                                       ]]

if args.data=='MNIST':
 PARAMS.append([28,28,1])
 PARAMS.append(['0','1','2','3','4','5','6','7','8','9'])

# PARAMS[0][0 ]: DATASET TYPE
# PARAMS[0][1 ]: NETWORK VERSION
# PARAMS[0][2 ]: SCALING SCHEME
# PARAMS[0][3 ]: ENABLE GPU
# PARAMS[0][4 ]: BATCH SIZE 
# PARAMS[0][5 ]: EPOCHS TRAIN
# PARAMS[0][6 ]: EVERY THIS MANY STEPS TAKE ACTION DURING TRAINING
# PARAMS[0][7 ]: EPOCHS TEST
# PARAMS[0][8 ]: EVERY THIS MANY STEPS TAKE ACTION DURING TESTING
# PARAMS[0][9 ]: BATCH BUFFER CAPACITY
# PARAMS[0][10]: GAN LATENT DIMENSION
# PARAMS[1][* ]: DATASET SHAPE 
# PARAMS[2][* ]: DATASET CLASSES


