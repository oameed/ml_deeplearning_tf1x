####################################
### DEEP LEARNING WITH TF1X      ###
### UTILITY FUNCTION DEFINITIONS ###
### by: OAMEED NOAKOASTEEN       ###
####################################

import os
import sys
import numpy                  as np
from matplotlib import pyplot as plt

def getFILENAMES(PATH,COMPLETE=True):
 filename=[]
 for FILE in os.listdir(PATH):
  if not FILE.startswith('.'):
   if COMPLETE:
    filename.append(os.path.join(PATH,FILE)) 
   else:
    filename.append(FILE)
 return filename

def ckptREAD(CKPTPATH,PRINT=False):
 f=open(os.path.join(CKPTPATH,'checkpoint'),'r')
 fl=f.readlines()
 CKPT=fl[0].split('"')[1]
 if PRINT:
  print(' READING CHECKPOINT: ',CKPT)
 return CKPT

def read_EA_Scalar(EA,SCALAR):
 data  =EA.Scalars(SCALAR) 
 scalar=np.array([(i.step,i.value) for i in data])
 return scalar

def plotter(X,PATH,MODE):
 if MODE=='grid':
  fig,ax =plt.subplots(10,10)
  plt.subplots_adjust(left=0,right=1,bottom=0,top=1,wspace=0.001,hspace=0.075)
  for  counter_r,row in enumerate(ax ):
   for counter_c,col in enumerate(row):
    col.imshow(X[counter_r*10+counter_c],cmap='gray')
    col.axes.axis('off')
  plt.savefig(os.path.join(PATH,'predictions'+'.png'),format='png')
 else:
  if MODE=='metrics':
   plt.figure()
   plt.plot(X[0][:,0]/1e3,X[0][:,1]/np.amax(X[0][:,1]),'b',linewidth=2, label='d_loss')
   plt.plot(X[1][:,0]/1e3,X[1][:,1]/np.amax(X[1][:,1]),'r',linewidth=2, label='g_loss')
   axes=plt.gca()
   axes.set_ylim([0,1])
   plt.title('Normalized Training Loss')
   plt.xlabel('Iterations (K)') 
   plt.legend()
   plt.grid()
   plt.savefig(os.path.join(PATH,'losses'+'.png'), format='png')


