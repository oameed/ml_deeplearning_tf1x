###################################################
### DEEP LEARNING WITH TF1X                     ###
### TRAIN GENERATIVE ADVERSESRIAL NETWORK (GAN) ### 
### MODEL DEFINITIONS                           ###
### by: OAMEED NOAKOASTEEN                      ###
###################################################

import os
import sys
import numpy      as np
import tensorflow as tf
sys.path.append(os.path.join(sys.path[0],'..'))
from layersd import (layer_conv,
                     layer_cvts,
                     layer_flcn )

def discriminator(X,NAME ,MODE):
 with tf.name_scope('Discriminator'+'_'+NAME):
  x  =layer_conv   (X      ,128,[2,2]                 ,'dsr_CONV_1',MODE           )
  x  =layer_conv   (x      ,128,[2,2]                 ,'dsr_CONV_2',MODE           )
  x  =tf.reshape   (x      ,[x.get_shape().as_list()[0],-1]                        )
  x  =layer_flcn   (x      ,1                         ,'dsr_FLCN_1',MODE,False,True)
 return x

def generator    (Z,SHAPE,MODE):
 with tf.name_scope('Generator'):
  x  =layer_flcn   (Z      ,    7*7*128 ,'gnr_FLCN_1'      ,MODE           )
  x  =tf.reshape   (x      ,[-1,7,7,128]                                   )  
  x  =layer_cvts   (x      ,        128 ,'gnr_CONV_TRANS_1',MODE           )
  x  =layer_cvts   (x      ,          1 ,'gnr_CONV_TRANS_2',MODE,True ,True)
  x  =tf.nn.sigmoid(x                                                      )
 return x 

def loss(REAL,FAKE):
 with tf.name_scope('Loss'):
  d_loss_real = tf.reduce_mean(tf.nn.sigmoid_cross_entropy_with_logits(labels=tf.ones_like (REAL), logits=REAL))
  d_loss_fake = tf.reduce_mean(tf.nn.sigmoid_cross_entropy_with_logits(labels=tf.zeros_like(FAKE), logits=FAKE))
  d_loss      = d_loss_real+d_loss_fake
  g_loss      = tf.reduce_mean(tf.nn.sigmoid_cross_entropy_with_logits(labels=tf.ones_like (FAKE), logits=FAKE))
 tf.compat.v1.add_to_collection(tf.compat.v1.GraphKeys.ACTIVATIONS,d_loss)
 tf.compat.v1.add_to_collection(tf.compat.v1.GraphKeys.ACTIVATIONS,g_loss)
 return d_loss, g_loss


